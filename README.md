#### TP3 ISI Cryptographie

Binôme: Julien Vanryssel, Jarod Vanderlynden


#### Comment Executer le programme
dans le dossier isi-tp3-crypto
lancer
####  python3 init.py
Le programme va vous demander 2 mot de passe pour les 2 responsables et s'arreter.
Pour la suite lancer
####  python3 crypto.py
Le programme va vous demander les mots de passes que vous venez d'entrer. Si ils sont bon le programme va vous demander de taper un numéro pour éxécuter les commandes pour ajouter ou supprimer des données.
Pour les représentents
####  python3 representant.py
Le programme va vous demander le mot de passe d'un responsable et a besoin de sa clé USB. Si il est correcte vous drevez rentrer un mot de passe pour le représentant et ça va le créer.



#### Question 1

Pour réaliser cette solution de déchiffrement nous avons pensé à:
Chacun des 2 référents ont sur leur clé USB une clé cryptographique chiffrée grâce a leurs mot de passe (préalablement hashé) respectif.
Après avoir demandé leur mot de passe et verifié leur validité, on va récupérer les clés cryptographiques et les déchiffrés grâce aux mots de passe récupérés, sur leurs clés USB, si l'accès y est impossible (peu importe la raison) la mise en service est impossible. 
Une fois les 2 clés des 2 responsables récupérés en les ajoutant ensemble on obtient une dernière clé unique qui est celle qui permet de déchiffrer le fichier de données.


#### Question 2

Nous avons mis en place le système que nous avions imaginé lors de la question 1. Toutes les informations permettant la récupération de la clé servant a déchiffrer le fichier de donnés sont stocké chiffré et cette clé n'est récupérable seulement lorsque les 2 réprésentants entrent leur mot de passe, et que leurs clé usb sont bien présente (du moins les fichiers contenant leurs clé personnel).
Certains problème subsistent, lors de l'ajout ou de la suppression de donnés, le fichier de donné se retrouve stocké temporairement en clair dans le RAM Disk (Ce qui peut poser problème si celui ci venait a être volé a ce moment précis) 
Tous les fichiers nécéssaires au bon fonctionnement sont crée lors de l'initialisation (programme init) et les mots de passe des responsable et les clés sont générer a ce moment la (Le relancer rend le fichier de données inutilisable)

#### Question 3

Pour la délégation de droits, nous avons ajouté la possibilité pour un responsable de lancer un programme pour s'ajouter un représentant légal. (Et uniquement 1 seul représentant légal)
Un réprésentant d'un résponsable dispose d'une clé USB (avec une clé de chiffrement identique a celle du responsable mais chiffré differement) et d'un mot de passe qui lui son propres. Il peut donc se substituer au responsable mais le service fait bien la distinction entre ce représentant et le résponsable.

#### Question 4

Le programme representant.py permet après qu'un responsable ai entré son mot de passe, de crée un nouveau représentant pour ce responsable. Si le programme est relancé les informations du précédent représentant sont écrasés.

#### Question 5

Pour cette question nous nous somems retrouvés façe a un problème que nous avons pas su résoudre. 
Tout d'abord pour réaliser une révocation de droit nous avons pensé qu'il fallait déchiffrer le fichier de donnés et le rechiffrer avec de toutes nouvelles clé et mot de passes. Les mots de passes peuvent rester identique c'est surtout la nouvelle génération de clé qui est important ici car c'est ce qui permet d'être sur que l'utilisateur qui a été révoqué n'aura plus d'accès possible aux donées.
Pour réaliser cette révocation il est nécéssaire d'avoire Le responsable 1 ou son représentant et le responsable 2 ou son représentant. Mais la présence de la 3ème personne n'est pas nécéssaire.
Seulement demander les 3 mots de passes des utilisateurs qui veulent révoquer un tiers est insuffisant car la sécurité de cette manipulation reposerait alors sur la sécurité du programme ce qui est contraire a ce qui est demandé dans ce TP.
Notre solution nous semble pertinente car elle permet d'être sur d'empecher a la personne révoqué de pouvoir aider a acceder a la base de données mais elle ne correspond pas a ce qui est demandé dans le sujet car ne nécéssite que 2 personnes présentes. Face a cette impasse nous avons essayer de trouver une autre solution, mais nous n'y sommes pas arrivé.


#### Conclusion

Notre système pour les question 1-2-3-4 nous semble correspondre a ce qui est demandé dans le sujet et fonctionne correctement. 
Si nous avions eu plus de temps nous aurions surement implémenter notre solution de la question 5, même si celle-ci ne correspond pas exactement au sujet. Et nous aurions peut être trouvé une solution a notre problème entre temps. De plus nous aurions pu essayer de trouver une solution pour éviter que le fichier de donnés se retrouve stocké temporairement en clair dans le RAM Disk. Même si cette manipulation n'a lieu que quand le service est en marche et donc que les utilisateurs se sont authentifié correctement. 
