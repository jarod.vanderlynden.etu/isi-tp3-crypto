import base64
import hashlib
import os
import random
#Attention le lancement de ce script écrase data.txt
#Et réinitialise les représentants.
#Ce programme n'est donc a lancer qu'un fois uniquement, si il est relancé on recommence la base de 0
os.popen('rm -rf datatxt')
os.popen('touch datatxt')
os.popen("./initialisation.sh")

print("Entrez le mot de passe Admin1\n")
mdp = input()
print("Entrez le mot de passe Admin2\n")
mdp2 = input()
mdpH = (base64.b64encode(mdp.encode("utf-8")))
mdpH = hashlib.sha256(mdpH).hexdigest()
mdp2H = (base64.b64encode(mdp2.encode("utf-8")))
mdp2H = hashlib.sha256(mdp2H).hexdigest()
tab = ["A","B","C","D","E","F","0","1","2","3","4","5","6","7","8","9"]
cle=""
cle2=""
for i in range(32):
    a = random.randint(0,15)
    cle = cle + tab[a]
    a = random.randint(0,15)
    cle2 = cle2 + tab[a]
#os.popen('echo "'+cle+'" > cleusb1/clecrypt1.txt')
os.popen("echo -n "+ cle+" |openssl enc -aes-256-ecb -out cleusb1/clecrypt1.txt -K "+mdpH)
os.popen("echo -n "+ cle2+" |openssl enc -aes-256-ecb -out cleusb2/clecrypt2.txt -K "+mdp2H)
mdpH = (base64.b64encode(mdpH.encode("utf-8")))
mdpH = hashlib.sha256(mdpH).hexdigest()
mdp2H = (base64.b64encode(mdp2H.encode("utf-8")))
mdp2H = hashlib.sha256(mdp2H).hexdigest()
os.popen('echo "'+mdpH+'" > mdp/mdpHash1.txt')
os.popen('echo "'+mdp2H+'" > mdp/mdpHash2.txt')
