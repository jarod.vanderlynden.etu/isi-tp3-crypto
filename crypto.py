import base64
import hashlib
import os
import time

def GetUSBKeys(k1,k2,flag):
    if flag==[0,0]:
        nameFile1 = "cleusb1/clecrypt1.txt"
        nameFile2 = "cleusb2/clecrypt2.txt"
    elif flag==[0,1]:
        nameFile1 = "cleusb1/clecrypt1.txt"
        nameFile2 = "cleusbRep2/clecryptRep2.txt"
    elif flag==[1,0]:
        nameFile1 = "cleusbRep1/clecryptRep1.txt"
        nameFile2 = "cleusb2/clecrypt2.txt"
    elif flag==[1,1]:
        nameFile1 = "cleusbRep1/clecryptRep1.txt"
        nameFile2 = "cleusbRep2/clecryptRep2.txt"
    else:
        print('error')
        exit()
    try:
        f = open (nameFile1,"r")
    except:
        print('Veuillez insérer votre clé USB')
        return -1
    f.close()
    try:
        f = open (nameFile2,"r")
    except:
        print('Veuillez insérer votre clé USB')
        return -1
    f.close()
    os.popen("openssl enc -d -aes-256-ecb -in "+ nameFile1+" -out RAMDisk/cle.txt -K "+k1)
    os.popen("openssl enc -d -aes-256-ecb -in "+ nameFile2+" -out RAMDisk/cle2.txt -K "+k2)
    #Due a un problème l'ouverture du fichier se faisait avant qu'il soit crée.
    time.sleep(0.1)
    f = open('RAMDisk/cle.txt')
    g = open('RAMDisk/cle2.txt')
    cles = f.readlines()[0],g.readlines()[0]
    f.close()
    g.close()
    os.popen("rm -rf RAMDisk/*")
    return cles

def startServeur(cles):
    cleU = cles[0]+cles[1]
    cleU = (base64.b64encode(cleU.encode("utf-8")))
    cleU = hashlib.sha256(cleU).hexdigest()
    while True:
        print("Appuyez sur\n 1-Ajouter un couple carte user\n 2-supprimer un couple carte/user\n 3-recherchez un numéro de carte\n 4- Eteindre le serveur")
        k = input()
        if (k[0]=='1'):

            name = input("entrez le nom du client\n")
            num = input("entrez le numéro de carte du client\n")
            x = name + "*" + num + "/"
            #os.popen("echo -n "+ x+" |openssl enc -aes-256-ecb -out RAMDisk/temp.txt -K "+ cleU)
            os.popen("echo -n "+ x+" > RAMDisk/temp.txt")
            time.sleep(0.1)
            os.popen("openssl enc -d -aes-256-ecb -in datatxt -out RAMDisk/data.txt -K "+cleU)
            time.sleep(0.1)
            os.popen("cat RAMDisk/temp.txt >> RAMDisk/data.txt")
            time.sleep(0.1)
            os.popen("openssl enc -aes-256-ecb -in RAMDisk/data.txt -out datatxt -K "+ cleU)
            time.sleep(0.1)
            #Due a un problème l'ouverture du fichier se faisait avant qu'il soit crée.
            #time.sleep(0.1)
            #os.popen("truncate -s -2 RAMDisk/temp.txt")
            #os.popen("cat RAMDisk/temp.txt | tr -d '\n' >> data.txt")
            #time.sleep(0.1)
            os.popen("rm -rf RAMDisk/*")
        elif (k[0]=='2'):
            name = input("entrez le nom du client a effacer\n")
            num = input("entrez le numéro de carte du client\n")
            x = name + "*" + num
            os.popen("openssl enc -d -aes-256-ecb -in datatxt -out RAMDisk/data.txt -K "+cleU)
            time.sleep(0.1)
            f = open("RAMDisk/data.txt","r")
            data = f.readlines()[0]
            data = data.split('/')
            newdata = ''
            for i in range(len(data)-1):
                if (data[i] != x):
                    newdata = newdata + data[i]+"/"
                else:
                    print("a supprimé le client "+data[i])
            f.close()
            f = open("RAMDisk/data.txt","w")
            f.write(newdata)
            f.close()
            os.popen("openssl enc -aes-256-ecb -in RAMDisk/data.txt -out datatxt -K "+ cleU)
            os.popen("rm -rf RAMDisk/*")
        elif (k[0]=='3'):
            name = input("entrez le nom du client pour lequel il vous faut les numéros de carte\n")
            os.popen("openssl enc -d -aes-256-ecb -in datatxt -out RAMDisk/data.txt -K "+cleU)
            time.sleep(0.1)
            f = open("RAMDisk/data.txt","r")
            data = f.readlines()[0]
            data = data.split('/')
            printdata = ''
            for i in data:
                x=i.split('*')
                if x[0] == name:
                    printdata = printdata + x[1] + "/"
            print("Voici les numéros de carte du client demandé: \n"+printdata+"\n")
        elif(k[0]=='4'):
            return 0


#Pour le flag 0 = Responsable, 1= Représentant
def start(mdp1,mdp2,mdp3,mdp4):
    f.close()
    print("entrez votre mot de passe Admin1 ou Représentant1")
    mdp = input()
    mdp = (base64.b64encode(mdp.encode("utf-8")))
    mdpK = hashlib.sha256(mdp).hexdigest()
    mdp = (base64.b64encode(mdpK.encode("utf-8")))
    mdp = hashlib.sha256(mdp).hexdigest()
    #Les mot de passes de User 1 et 2 sont mdp1 et mdp2
    if (mdp == mdp1[0][:-1]):
        print("success Admin1, Admin2 or rep2 password:")
        mdp = input()
        mdp = (base64.b64encode(mdp.encode("utf-8")))
        mdpK2 = hashlib.sha256(mdp).hexdigest()
        mdp = (base64.b64encode(mdpK2.encode("utf-8")))
        mdp = hashlib.sha256(mdp).hexdigest()
        if (mdp == mdp2[0][:-1]):
            print("Recherche Clé usb 1 et 2")
            flag = [0,0]
            cles = GetUSBKeys(mdpK,mdpK2,flag)
            if cles == -1:
                exit()
            else:
                startServeur(cles)
        elif(mdp == mdp4):
            print("Recherche Clé usb 1 et 2")
            flag=[0,1]
            cles = GetUSBKeys(mdpK,mdpK2,flag)
            if cles == -1:
                exit()
            else:
                startServeur(cles)
        else:
            print("error")
    elif (mdp == mdp3):
        print("success Rep1, Admin2 or rep2 password:")
        mdp = input()
        mdp = (base64.b64encode(mdp.encode("utf-8")))
        mdpK2 = hashlib.sha256(mdp).hexdigest()
        mdp = (base64.b64encode(mdpK2.encode("utf-8")))
        mdp = hashlib.sha256(mdp).hexdigest()
        if (mdp == mdp2[0][:-1]):
            print("Recherche Clé usb 1 et 2")
            flag = [1,0]
            cles = GetUSBKeys(mdpK,mdpK2,flag)
            if cles == -1:
                exit()
            else:
                startServeur(cles)
        elif(mdp == mdp4):
            print("Recherche Clé usb 1 et 2")
            flag=[1,1]
            cles = GetUSBKeys(mdpK,mdpK2,flag)
            if cles == -1:
                exit()
            else:
                startServeur(cles)
        else:
            print("error")
    else:
        print("Wrong password fail")



f = open("mdp/mdpHash1.txt","r")
mdp1 = f.readlines()
f.close()
f= open("mdp/mdpHash2.txt","r")
mdp2 = f.readlines()
f.close()
try:
    f= open("mdp/mdpHashRep1.txt","r")
    mdp3 = f.readlines()
    mdp3 = mdp3[0][:-1]
    f.close()
except:
    mdp3=-1

try:
    f= open("mdp/mdpHashRep2.txt","r")
    mdp4 = f.readlines()
    mdp4 = mdp4[0][:-1]
    f.close()
except:
    mdp4=-1

start(mdp1,mdp2,mdp3,mdp4)
